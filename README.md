# TS Movies

A simple movie-list application using MovieDB API.

- Search movies
- Popular, upcoming and top-rated movies
- Saving favorites movies
- Infinite scrolling
- Language localisation support
- Light and dark theme
- Film rating system

## How to run

1. `git clone https://INTJT@bitbucket.org/INTJT/ts-movies.git`
2. `npm install`
3. `npm run dev`
