import { ObservableList, ObservableValue, ValueEvents } from '../observable';
import { IMovie } from '../movie';
import { SettingsHelper } from './settingsHelper';
import { popularStrategy, ScrollStrategy } from '../scrollStrategy';
import { Param } from './apiHelper';

export class ListHelper {

    static readonly movies: ObservableList<IMovie> = new ObservableList<IMovie>();

    private static page = 0;
    static scrollStrategy: ObservableValue<ScrollStrategy> =
        new ObservableValue<ScrollStrategy>(popularStrategy);

    static clear(): void {
        ListHelper.movies.clear();
        ListHelper.page = 0;
    }

    static initialize(): void {

        ListHelper.scrollStrategy.subscribe(ValueEvents.set, () => {
            this.clear();
            this.loadMore()
                .then();
        });

    }

    static loadMore(): Promise<boolean> {

        const apiParams: Param[] = [
            ["page", (ListHelper.page + 1).toString()],
            ["language", SettingsHelper.language.value]
        ];

        if(SettingsHelper.region !== undefined) apiParams.push(["region", SettingsHelper.region]);

        return ListHelper.scrollStrategy.value(apiParams)
            .then(
                response => {
                    if(Array.isArray(response)) {
                        response.forEach(movie => {
                            if(!ListHelper.movies.find(item => item.id === movie.id)) {
                                ListHelper.movies.add(movie);
                            }
                        });
                        ListHelper.page++;
                        if (response.length === 0) return  Promise.reject(false);
                        else return Promise.resolve(true);
                    }
                    else return Promise.reject(false)
                },
                () => Promise.reject(false)
            );
    }

}
