import { IMovie } from '../movie';
import { SettingsHelper } from './settingsHelper';

interface IMovieDBApiResponse {
    id: number,
    title: string,
    overview: string,
    poster_path?: string,
    backdrop_path?: string,
    release_date?: string,
    adult: boolean
}

export type Param = [key: string, value: string];

export interface IError {
    code: number,
    message: string
}

export class ApiHelper {

    private static readonly movieApiKey: string = "92722b1ccaba8f53d758caedf294763b";
    private static readonly movieApiPath: string = "https://api.themoviedb.org/3";

    static apiFetch(
        path: string,
        params: Param[] = []
    ): Promise<Response> {

        const query =
            [["api_key", ApiHelper.movieApiKey], ...params]
                .map(param => `${param[0]}=${param[1]}`)
                .join("&");

        return fetch(`${ApiHelper.movieApiPath}/${path}?${query}`, { method: "GET" });

    }

    static getMovies(
        path: string,
        params: Param[] = []
    ): Promise<IMovie[] | IError> {

        return ApiHelper.apiFetch(path, params)
            .then(response => {
                if(response.status === 200) {
                    return response.json().then(body => body.results.filter((movie: IMovieDBApiResponse) => {
                        if(SettingsHelper.adult.value) return true;
                        else return !movie.adult;
                    }).map(ApiHelper.movieMapper));
                }
                else {
                    return response.text()
                        .then(text => ({
                            code: response.status,
                            message: text
                        } as IError));
                }
            });

    }

    static searchMovies(
        query: string,
        params: Param[] = []
    ): Promise<IMovie[] | IError> {

        return ApiHelper.getMovies(
            "search/movie",
            [
                ...params,
                ["query", query]
            ]
        );

    }

    static getMovie(
        id: number,
        params: Param[] = []
    ): Promise<IMovie | IError> {

        return ApiHelper.apiFetch(
            `movie/${id}`,
            [...params]
        ).then(response => {
            if (response.status === 200) {
                return response.json().then(body => ApiHelper.movieMapper(body))
            }
            else {
                return response.text()
                    .then(text => ({
                        code: response.status,
                        message: text
                    } as IError));
            }
        })

    }

    private static movieMapper(apiResult: IMovieDBApiResponse): IMovie {
        return {
            id: apiResult.id,
            title: apiResult.title,
            overview: apiResult.overview,
            posterUrl: apiResult.poster_path,
            backdropUrl: apiResult.backdrop_path,
            date: apiResult.release_date ? new Date(apiResult.release_date) : undefined
        }
    }

}
