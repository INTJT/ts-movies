import { FunctionComponent, div, h1, img, p, small } from './domHelper';
import { FavoritesHelper } from './favoritesHelper';
import { IMovie } from '../movie';

const shadowedCard = div({ className: ["card", "shadow-sm"] });
const cardBody = div({ className: "card-body" });
const cardText = p({ className: ["card-text", "truncate"] });
const cardTitle = h1({ className: "card-title" });
const cardFooter = div({ className: ["d-flex", "justify-content-between", "align-items-center"] });
const smallText = small({ className: "text-muted" });

const randomBody = div({ className: ["col-lg-6", "col-md-8", "mx-auto", "random-item-body"] })
const randomTitle = h1({ className: ["text-light"] });
const randomText = p({ className: ["lead", "text-white"] });

const imagePath = "https://image.tmdb.org/t/p";

function movieImage(src: string | undefined, size: string): Element {
    return img({
        className: `movie-image`,
        attributes: {
            src: src ? `${imagePath}/${size}/${src}` : "images/no-image.jpg",
            alt: ""
        }
    })();
}

function icon(movie: IMovie): Element {

    const icon = img({
        className: ["bi", "bi-heart-fill", "position-absolute", "p-2", "movie-favorite-icon"],
        attributes: {
            src: FavoritesHelper.isFavorite(movie.id) ? "icons/like.svg" : "icons/non-like.svg",
            alt: ""
        }
    })() as HTMLElement;

    icon.onclick = () => {
        const findMovie = FavoritesHelper.favorites.find(item => item.id === movie.id);
        if (findMovie) FavoritesHelper.favorites.remove(findMovie);
        else FavoritesHelper.favorites.add(movie);
    }

    return icon;
}

function movieWrapper(classes: string | string[]) {
    return function(id: number): FunctionComponent {
        return div({
            className: classes,
            attributes: {
                ["data-movie-id"]: id.toString()
            }
        })
    }
}

export class MovieRenderHelper {

    static readonly listItem: (id: number) => FunctionComponent =
        id => movieWrapper(["col-lg-3", "col-md-4", "col-12", "p-2"])(id);

    static readonly favoriteItem: (id: number) => FunctionComponent =
        id => movieWrapper(["col-12", "p-2"])(id);

    static readonly randomItem: (id: number) => FunctionComponent =
        id => movieWrapper([])(id);

    static movieToCard(movie: IMovie): Element {
        return shadowedCard(
                icon(movie),
                movieImage(movie.posterUrl, "w342"),
                cardBody(
                    cardTitle(movie.title || "???"),
                    cardText(movie.overview || "no data"),
                    cardFooter(smallText(movie.date ? movie.date.toDateString() : "no date"))
                )
            );
    }

    static movieToRandom(movie: IMovie): Element {
        return MovieRenderHelper.randomItem(movie.id)(
            movieImage(movie.backdropUrl || movie.posterUrl, "original"),
            icon(movie),
            randomBody(
                randomTitle(movie.title || "???"),
                randomText(movie.overview || "no data")
            )
        );
    }

}