import { ObservableValue, ValueEvents } from '../observable';
import { LocalStorageKeys } from '../localStorageKeys';

export enum Theme {
    light = "light",
    dark = "dark",
}

export class SettingsHelper {

    static language: ObservableValue<string> =
        new ObservableValue<string>(localStorage.getItem(LocalStorageKeys.language) || "en-US");

    static theme: ObservableValue<Theme> =
        new ObservableValue<Theme>(localStorage.getItem(LocalStorageKeys.theme) === "dark" ? Theme.dark : Theme.light);

    static adult: ObservableValue<boolean> =
        new ObservableValue<boolean>(localStorage.getItem(LocalStorageKeys.adult) === "true");

    static region: string | undefined = undefined;

    static initialize(): void {
        SettingsHelper.language.subscribe(ValueEvents.set, () => {
            localStorage.setItem(LocalStorageKeys.language, SettingsHelper.language.value);
            window.location.reload();
        });
        SettingsHelper.theme.subscribe(ValueEvents.set, () => {
           localStorage.setItem(LocalStorageKeys.theme, SettingsHelper.theme.value.toString());
        });
        SettingsHelper.adult.subscribe(ValueEvents.set, () => {
            localStorage.setItem(LocalStorageKeys.adult, SettingsHelper.adult.value.toString());
        });
    }

}
