import { ListEvents, ObservableList } from '../observable';
import { IMovie } from '../movie';
import { ApiHelper } from './apiHelper';
import { SettingsHelper } from './settingsHelper';
import { LocalStorageKeys } from '../localStorageKeys';

if(localStorage.getItem(LocalStorageKeys.favorites) === null) {
    localStorage.setItem(LocalStorageKeys.favorites, "[]");
}

export class FavoritesHelper {

    static readonly favorites: ObservableList<IMovie> = new ObservableList<IMovie>();

    static initialize(): void {

        FavoritesHelper.favorites.subscribe(ListEvents.add, FavoritesHelper.overrideLocalStorage);
        FavoritesHelper.favorites.subscribe(ListEvents.replace, FavoritesHelper.overrideLocalStorage);
        FavoritesHelper.favorites.subscribe(ListEvents.remove, FavoritesHelper.overrideLocalStorage);

        const favoritesId: number[] =  JSON.parse(localStorage.getItem(LocalStorageKeys.favorites) as string) as number[];

        for(const id of favoritesId) {
            ApiHelper.getMovie(id, [
                ["language", SettingsHelper.language.value.toString()]
            ])
                .then(response => {
                    if(response) {
                        FavoritesHelper.favorites.add(response as IMovie);
                    }
                });
        }

    }

    static isFavorite(id: number): boolean {
        return FavoritesHelper.favorites
            .origin
            .find((movie: IMovie) => movie.id === id) !== undefined;
    }

    private static overrideLocalStorage(): void {
        localStorage.setItem(LocalStorageKeys.favorites, JSON.stringify(
            FavoritesHelper.favorites.origin
                .map(movie => movie.id)
        ));
    }

    static removeElements(id: number, root: Element = document.body): void {
        root.querySelectorAll(`[data-movie-id="${id}"]`).forEach(element => {
            element.parentElement?.removeChild(element);
        });
    }

    static updateIcons(id: number, root: Element = document.body): void {
        root.querySelectorAll(`[data-movie-id="${id}"] img.position-absolute`).forEach(element => {
            (element as HTMLImageElement).src = FavoritesHelper.isFavorite(id) ? "icons/like.svg" : "icons/non-like.svg"
        });
    }

}
