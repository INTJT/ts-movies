export type DomElement = string | Element;

//Very cheap version of functional components

export interface ComponentArgs {
    className?: string | string[];
    id?: string;
    attributes?: {
        [attribute: string]: string
    };
}

export type FunctionComponent = (...inner: DomElement[]) => Element;

export class DomHelper {

    private static domElementToNode(domElement: DomElement): Node {
        if(typeof domElement === "string") return document.createTextNode(domElement);
        else return domElement;
    }

    static createTag(tag: string):
        (componentArgs: ComponentArgs)
            => FunctionComponent {
        return function(attributes: ComponentArgs) {
            return function(...inner: DomElement[]) {

                const element = document.createElement(tag);

                if (attributes.className !== undefined) {
                    if (Array.isArray(attributes.className)) element.classList.add(...attributes.className);
                    else element.classList.add(attributes.className);
                }

                if(attributes.id !== undefined) element.id = attributes.id;

                if(attributes.attributes) {
                    for(const [attribute, value] of Object.entries(attributes.attributes)) {
                        element.setAttribute(attribute, value);
                    }
                }

                inner
                    .map(DomHelper.domElementToNode)
                    .forEach(node => element.appendChild(node));

                return element;

            }
        }
    }

}

export const div = DomHelper.createTag("div");
export const h1 = DomHelper.createTag("h1");
export const p = DomHelper.createTag("p");
export const img = DomHelper.createTag("img");
export const small = DomHelper.createTag("small");
