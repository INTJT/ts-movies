export interface IMovie {
    readonly id: number,
    readonly title: string,
    readonly overview?: string,
    readonly posterUrl?: string,
    readonly backdropUrl?: string,
    readonly date?: Date
}
