export enum LocalStorageKeys {
    favorites = "favorites",
    language = "language",
    theme = "theme",
    adult = "adult"
}
