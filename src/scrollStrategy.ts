import { ApiHelper, IError, Param } from './helpers/apiHelper';
import { IMovie } from './movie';
import { SettingsHelper } from './helpers/settingsHelper';

export type ScrollStrategy = (params: Param[]) => Promise<IMovie[] | IError>;

export const popularStrategy: ScrollStrategy =
    params => ApiHelper.getMovies('movie/popular', params);

export const upComingStrategy: ScrollStrategy =
    params => ApiHelper.getMovies('movie/upcoming', params);

export const topRatedStrategy: ScrollStrategy =
    params => ApiHelper.getMovies('movie/top_rated', params);

export function searchStrategy(query: string): ScrollStrategy {
    return params => ApiHelper.searchMovies(query, [
        ...params,
        ["include_adult", SettingsHelper.adult.value.toString()]
    ]);
}
