export function getRegion(): Promise<string | undefined> {
    return fetch("http://ip-api.com/json")
        .then(
            response => response.json().then(result => result.countryCode),
            () => undefined
        );
}
