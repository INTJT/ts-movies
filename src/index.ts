import { FavoritesHelper } from './helpers/favoritesHelper';
import { ListEvent, ListEvents, ObservableValue, ValueEvents } from './observable';
import { IMovie } from './movie';
import { MovieRenderHelper } from './helpers/movieRenderHelper';
import { SettingsHelper, Theme } from './helpers/settingsHelper';
import { ListHelper } from './helpers/listHelper';
import { ScrollStrategy, popularStrategy, topRatedStrategy, upComingStrategy, searchStrategy } from './scrollStrategy';
import { getRegion } from './region';

export async function render(): Promise<void> {

    SettingsHelper.region = await getRegion();

    //setup strategies;
    {

        let lastCategoryStrategy = ListHelper.scrollStrategy.value;

        const popularButton = document.getElementById("popular") as HTMLInputElement;
        const upcomingButton = document.getElementById("upcoming") as HTMLInputElement;
        const topRatedButton = document.getElementById("top_rated") as HTMLInputElement;
        const search = document.getElementById("search") as HTMLInputElement;
        const submit = document.getElementById("submit") as HTMLButtonElement;

        const setCategoryStrategy: (strategy: ScrollStrategy) => () => void =
            (strategy: ScrollStrategy) => () => {
                if (ListHelper.scrollStrategy.value !== strategy) {
                    ListHelper.scrollStrategy.value = strategy;
                    lastCategoryStrategy = strategy;
                    search.value = "";
                }
            }

        const setSearch = (): void => {
            ListHelper.scrollStrategy.value = search.value ? searchStrategy(search.value) : lastCategoryStrategy;
        }

        popularButton.onclick = setCategoryStrategy(popularStrategy);
        upcomingButton.onclick = setCategoryStrategy(upComingStrategy);
        topRatedButton.onclick = setCategoryStrategy(topRatedStrategy);

        search.onchange = setSearch;
        submit.onclick = setSearch;
        search.oncancel = setSearch;

    }

    //setup infinite loading
    {

        const loadMoreButton = document.getElementById("load-more") as HTMLButtonElement;
        const canLoad = new ObservableValue<boolean>(true);

        canLoad.subscribe(ValueEvents.set, () => {
            loadMoreButton.disabled = !canLoad.value;
            loadMoreButton.hidden = !canLoad.value;
        });

        canLoad.value = false;

        ListHelper.movies.subscribe(ListEvents.add, () => {
            if (!canLoad.value) canLoad.value = true;
        });

        ListHelper.movies.subscribe(ListEvents.clear, () => {
            canLoad.value = false;
        });

        const tryLoadMore = (): void => {
            if (canLoad) {
                canLoad.value = false;
                ListHelper.loadMore().then(() => {
                    canLoad.value = true;
                });
            }
        }

        const loadObserver = new IntersectionObserver(
            entries => {
                if (entries[0].isIntersecting) tryLoadMore();
            },
            { threshold: [0.5] }
        );
        loadObserver.observe(loadMoreButton);
        loadMoreButton.onclick = tryLoadMore;

    }

    //setup favorites list
    {

        const favoriteMovies = document.getElementById("favorite-movies") as HTMLElement;

        FavoritesHelper.favorites.subscribe(ListEvents.add, (event) => {
            const movieEvent = event as ListEvent<IMovie>;
            FavoritesHelper.updateIcons(movieEvent.item.id);
            favoriteMovies.appendChild(
                MovieRenderHelper.favoriteItem(movieEvent.item.id)(MovieRenderHelper.movieToCard(movieEvent.item))
            );
        });

        FavoritesHelper.favorites.subscribe(ListEvents.remove, (event) => {
            const movieEvent = event as ListEvent<IMovie>;
            FavoritesHelper.removeElements(movieEvent.item.id, favoriteMovies);
            FavoritesHelper.updateIcons(movieEvent.item.id);
        });

        FavoritesHelper.initialize();

    }

    const randomBlock = document.getElementById("random-movie") as HTMLElement;
    let prevItem: HTMLElement;

    function setRandom() {

        function replace() {
            const randomMovie: IMovie = ListHelper.movies.origin[Math.floor(Math.random() * ListHelper.movies.length)];
            const randomItem = MovieRenderHelper.movieToRandom(randomMovie) as HTMLElement;
            randomBlock.appendChild(randomItem);
            if (prevItem) randomBlock.removeChild(prevItem);
            prevItem = randomItem;
        }

        if (ListHelper.movies.length !== 0) {
            if (prevItem) {
                randomBlock.style.opacity = "0";
                setTimeout(() => {
                    replace();
                    randomBlock.style.opacity = "1";
                }, 1000);
            } else replace();
        }

    }

    //setup movies list
    {

        const filmContainer = document.getElementById("film-container") as HTMLElement;

        ListHelper.movies.subscribe(ListEvents.add, event => {
            const listEvent = event as ListEvent<IMovie>;
            filmContainer.appendChild(
                MovieRenderHelper.listItem(listEvent.item.id)(MovieRenderHelper.movieToCard(listEvent.item))
            );
        });

        ListHelper.movies.subscribe(ListEvents.clear, () => {
            filmContainer.querySelectorAll("[data-movie-id]")
                .forEach(element => {
                    filmContainer.removeChild(element);
                })
        });

        ListHelper.initialize();
        ListHelper.loadMore()
            .then(setRandom);

    }

    //setup random
    setInterval(setRandom, 10000);

    //setup settings
    {

        const languageSelect = document.getElementById("language") as HTMLSelectElement;
        const themeButton = document.getElementById("theme") as HTMLButtonElement;
        const adultButton = document.getElementById("adult") as HTMLButtonElement;

        const setTheme = (): void => {
            if (SettingsHelper.theme.value === Theme.dark) document.body.classList.add("dark");
            else document.body.classList.remove("dark");
        }

        const setAdult = (): void => {
            if (SettingsHelper.adult.value) adultButton.classList.add("include");
            else adultButton.classList.remove("include");
        }

        languageSelect.value = SettingsHelper.language.value;

        languageSelect.onchange = () => {
            SettingsHelper.language.value = languageSelect.value;
        };

        themeButton.onclick = () => {
            SettingsHelper.theme.value = SettingsHelper.theme.value === Theme.light ? Theme.dark : Theme.light;
        };

        adultButton.onclick = () => {
            SettingsHelper.adult.value = !SettingsHelper.adult.value;
        }

        SettingsHelper.theme.subscribe(ValueEvents.set, setTheme);
        SettingsHelper.adult.subscribe(ValueEvents.set, setAdult);

        SettingsHelper.initialize();
        setTheme();
        setAdult();

    }

}