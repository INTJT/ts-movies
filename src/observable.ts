export interface IEvent {
    type: string
}

type handler = (event: IEvent) => any;

export abstract class Observable {
    abstract subscribe(eventType: string, callback: handler): void
    abstract notify(event: IEvent): void
}

export class ObservableGroup extends Observable {

    private readonly groups: Map<string, Set<handler>> = new Map<string, Set<handler>>();

    constructor(groups: string[] = []) {
        super();
        for(const group of groups) {
            this.groups.set(group, new Set<handler>());
        }
    }

    subscribe(group: string, callback: handler): void {
        if(!this.groups.has(group)) {
            throw new Error(`Group "${group}" doesn't exist`);
        }
        (this.groups.get(group) as Set<handler>).add(callback);
    }

    notify(event: IEvent): void {
        if(!this.groups.has(event.type)) {
            throw new Error(`Group "${event.type}" doesn't exist`);
        }
        this.groups.get(event.type)
            ?.forEach(callback => callback(event))
    }

}



//Observable value

export interface ValueEvent<I> extends IEvent {
    oldValue: I,
    newValue: I
}

export enum ValueEvents {
    set = "set"
}

export class ObservableValue<I> extends ObservableGroup {

    private variable: I;

    constructor(value: I) {
        super([ValueEvents.set]);
        this.variable = value;
    }

    get value() { return this.variable; }

    set value(value: I) {
        if(this.variable !== value) {
            const oldValue = this.variable;
            this.variable = value;
            this.notify({
                type: ValueEvents.set,
                oldValue,
                newValue: this.variable
            } as ValueEvent<I>);
        }
    }

}



//Observable list

export interface ListEvent<I> extends IEvent {
    index: number,
    item: I
}

export enum ListEvents {
    add = "add",
    replace = "replace",
    remove = "remove",
    clear = "clear"
}

export class ObservableList<T> extends ObservableGroup {

    private readonly items: T[];

    constructor(items: T[] = []) {
        super([
            ListEvents.add,
            ListEvents.replace,
            ListEvents.remove,
            ListEvents.clear
        ]);
        this.items = items;
    }
    
    add(item: T): void {
        if (!this.items.includes(item)) {
            this.items.push(item);
            this.notify({
                type: ListEvents.add,
                index: this.length - 1,
                item
            } as ListEvent<T>);
        }
    }

    replace(index: number, item: T): void {
        if(index < 0 || index >= this.items.length) throw Error(`Bad index ${index}`);
        if(this.items[index] !== item) {
            this.items[index] = item;
            this.notify({
                type: ListEvents.replace,
                index, item
            } as ListEvent<T>)
        }
    }
    
    remove(item: T): void {
        const index = this.items.indexOf(item);
        if (index !== -1) {
            this.items.splice(index, 1);
            this.notify({
                type: ListEvents.remove,
                index, item
            } as ListEvent<T>);
        }
    }

    has(item: T): boolean {
        return this.items.includes(item);
    }

    find(predicate: (t: T) => boolean): T | undefined {
        return this.items.find(predicate);
    }

    findIndex(predicate: (t: T) => boolean): number {
        return this.items.findIndex(predicate);
    }

    clear(): void {
        if(this.length > 0) {
            this.items.splice(0, this.length);
            this.notify({
                type: ListEvents.clear
            });
        }
    }

    get length(): number {
        return this.items.length;
    }

    get origin(): T[] {
        return [...this.items];
    }

}
